package a.appium.manager;

import a.appium.android.AndroidDeviceConfiguration;
import java.io.IOException;


/**
 * Device Manager - Handles all device related information's e.g UDID, Model, etc
 */
public class DeviceManager {

    private static ThreadLocal<String> deviceUDID = new ThreadLocal<>();
    private AndroidDeviceConfiguration androidDeviceConfiguration;
    private String platform ="ANDROID";
    public DeviceManager() {
            androidDeviceConfiguration = new AndroidDeviceConfiguration();
    }

    public static String getDeviceUDID() {
        return deviceUDID.get();
    }

    protected static void setDeviceUDID(String UDID) {
        deviceUDID.set(UDID);
    }

    public String getMobilePlatform() {
            return platform;
    }

    public String getDeviceModel() throws InterruptedException, IOException {
        if (getMobilePlatform().equals("ANDROID")) {
            return androidDeviceConfiguration.getDeviceModel();
        }
        throw new IllegalArgumentException("DeviceModel is Empty");
    }

    public String getDeviceCategory() throws Exception {
            return androidDeviceConfiguration.getDeviceModel();
    }

    public String getDeviceVersion() {
            return androidDeviceConfiguration.deviceOS();
    }
}
