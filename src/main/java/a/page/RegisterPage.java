package a.page;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import a.model.PhoneDriver;

public class RegisterPage extends BasePage{
//	private PhoneDriver driver;
	public RegisterPage(PhoneDriver driver){
		super(driver);
		PageFactory.initElements(driver, this);
	}
	 @FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.paic.example.simpleapp:id/yzt_sdk_login_account']//android.widget.EditText")
//	 /android.widget.EditText[contains(@resource-id,'com.paic.example.simpleapp:id/comm_input_txt')&&contains(@password,false)]")///android.widget.TextView[@text='测试环境']/parent::/android.widget.Spinner
	 public WebElement accountId;
	 
	 @FindBy(xpath="//android.widget.FrameLayout[@resource-id='com.paic.example.simpleapp:id/yzt_sdk_login_pwd']//android.widget.EditText")
//android.widget.EditText[contains(@resource-id,'com.paic.example.simpleapp:id/comm_input_txt')&&contains(@password,true)]")
	 public WebElement codeId;
	 
	 @FindBy(id="com.paic.example.simpleapp:id/yzt_sdk_login_login_btn")
	 public WebElement logInButton;
	 
	 public boolean login(String username,String password){
		 boolean flag;
		 try{
		 accountId.click();
		 }catch(NoSuchElementException e){
			 System.out.println("没找到账号输入");
		 }
		 String text =accountId.getAttribute("name");
//		 System.out.println("????"+text+"????");
		 
//		 while(!accountId.getAttribute("name").equals("")){
//			 clearText(text);
//			 driver.getPageSource();
//			 text =accountId.getAttribute("name");
//		 }
		 
		 accountId.sendKeys(username);
		
//		
		 try{
			 codeId.click(); 
		 }catch (NoSuchElementException e) {
		 System.out.println("没找到密码输入");
		
		// TODO: handle exception
	}
//		 clickCentre(codeId);
		 codeId.sendKeys(password);
		 logInButton.click();
		 if(isExistByaId("请登录")){
			 flag=false;
		 }else{
			 flag=true;
		 }
		 return flag;
		 
	 }
	 private void clearText(String text){
			driver.pressKeyCode(123);
			for(int i=0;i<text.length();i++){
				driver.pressKeyCode(67);
			}
	}
	 public boolean isThisPage(){
		 return driver.getTitle().equals("一账通登录");
	 }
}
