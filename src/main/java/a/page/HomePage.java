package a.page;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import a.model.PhoneDriver;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class HomePage extends BasePage{
//	private PhoneDriver driver;//有这句话，会报错空指针
	EnterPage enter = new EnterPage(driver);
	RegisterPage register = new RegisterPage(driver);
//	public HomePage(PhoneDriver driver){
//		super(driver);
//		PageFactory.initElements(driver, this);
//	}
	 /**
	  * 查找serviceName,并点击clickName
	  * 默认值false，如果找到并且点击了，就true
	  */
	@AndroidFindBy(uiAutomator ="new UiSelector.description(\"全部\")")
	public WebElement list;

	 public HomePage(PhoneDriver driver) {
		// TODO Auto-generated constructor stub
		 super(driver);
	}
	public boolean clickTarget(String serviceName,String clickName){
		 boolean flag=false;
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 WebElement we =findCard(serviceName);
		 if(we!=null){
			 moveToAbove(we);
			 if(clickName.equals("")){
				 clickName=serviceName;
			 }
			 if(isExistByaId(clickName)){
				 driver.findElementByAccessibilityId(clickName).click();
				 flag=true;
			}
		 }
		 return flag;
	 }
	 public boolean isLogin(){
		 return !isExistByaId("请登录");
	 }
	 
	 public WebElement findCard(String aID) {
		WebElement we = null;
		int i=1;
		while(true){
			if(isExistByaId(aID)){
				we=driver.findElementByAccessibilityId(aID);
				break;
				}
			if(isExistByaId("没有发现心仪的内容？点击告诉我们您想看什么")){
				break;
				}else{
					swipeToUp(3000, 1);
					i++;
				}
			if(i>10){
					break;
				}
			}
				return we;
		}
	 /**
		 * 将cardName挪到屏幕上部
		 */
		 public boolean moveToAbove(WebElement we){
			 boolean flag = false;
				final TouchAction touchAction = new TouchAction(driver);
				int top=we.getLocation().y;
				if(we==null||top<0){
					return false;
				}
				if(we!=null){
					int h= getHeight();
					int w= getWidth();
					while((top>=h)||(top>=h/2)||(top<100)||(top==0)){
						if(top>=h){
							swipeToUp(3000, 1);
							driver.getPageSource();
							top=we.getLocation().y;
						}else if(top>=h/2){
							driver.swipe(w/2, top, w/2, 200, 3000);
							driver.getPageSource();
							top=we.getLocation().y;
						}else if (top<100&&top>0){
							driver.swipe(w/2, h/2, w/2, h/2+200, 3000);
							driver.getPageSource();
							top=we.getLocation().y;
						}else if(top==0){
							swipeToDown(3000, 1);
							driver.getPageSource();
							top=we.getLocation().y;
						}
					}
				}
				return flag;
			}
	 /**
	  * 点击更多菜单
	  */
	 public void clickMore(){
		 
	 }
	 /**
	  * 跳转对应页面
	  */
	 public void jumpToRegister(){
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 driver.findElementByAccessibilityId("请登录").click();
	 }
	 public void jumpToList(){
		 driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		 driver.findElementByAccessibilityId("全部").click();
	 }
	 public void jumpToSetting(){
		 
	 }
	 public boolean isThisPage() {
//		 return driver.getTitle().equals("用户中心");
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return isExistByaId("用户中心");
	 }
	 /**
	  * 返回当前页，返回失败时返回false
	  * @return
	 * @throws InterruptedException 
	  */
	 public boolean backtoThisPage() throws InterruptedException{
		 boolean flag = true;
		 int i =0;
		 while(!isThisPage()){
			 
			 i++;
			 if(isExistById("com.paic.example.simpleapp:id/loading")){
				 EnterPage enter = new EnterPage(driver);
				 enter.enterPage();
			 }else if(isExistByaId("关闭")){
				 driver.findElementByAccessibilityId("关闭").click();
			 }else{
				 pressEntityBack();
			 }
//			 pageSource();
			 if(i>3) 
				 flag =false;
		 }
		 return flag;
	 }
	 public void jumpToEnter(){
		 pressEntityBack();
	 }
	 /**
	  * 登出操作是否成功，登出成功，返回true
	  * @return
	  */
	 public boolean logout(){
		 boolean flag;
		 jumpToEnter();
		 enter.logout();
		 enter.jumpToUserCenter();
		 flag= !isLogin();
		 return flag;
	 }
	 public boolean login(String userName,String password){
		 boolean flag;
		 jumpToRegister();
		 if(register.login(userName, password)){
			 flag=true;
		 }else{
			 flag=false;
		 }
		 return flag;
	 }
}
