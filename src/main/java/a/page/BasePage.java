package a.page;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.sun.jna.platform.win32.WinNT.HANDLE;

import a.model.PhoneDriver;
import io.appium.java_client.TouchAction;

public class BasePage {
	PhoneDriver driver;
	
	private PhoneDriver getDriver() {
		return driver;
	}

	private void setDriver(PhoneDriver driver) {
		this.driver = driver;
	}
	public BasePage(PhoneDriver driver){
		if(driver!=null){
			setDriver(driver);
			PageFactory.initElements(driver, this);
		}else{
			System.out.println("driver is null");
		}
	}
//	public String url = "";
//	public String title="";
	/**
	 * 网页版操作
	 * @param driver
	 * @param url
	 */
//	public void init(PhoneDriver driver,String url){
//		driver.get(url);
//	}
//	
//	public void switchToPage(String url){
//		driver.get(url);
//	};
//	
//	public void quit(){
//		driver.quit();
//	};
	/**
	 * 页面的操作
	 * @return
	 */
	public String pageSource(){
		return driver.getPageSource();
	}
	public boolean isLoaded(String title){
		return driver.getTitle().contains(title);
	}
	public boolean isExistByaId(String accessibilityId){
		Boolean flag = true;
		try{
			driver.findElementByAccessibilityId(accessibilityId);
		}catch(NoSuchElementException e){
//			e.printStackTrace();
			System.out.println("没找到"+accessibilityId);
			flag=false;
		}
		return flag;
	}
	public boolean isExistById(String id){
		Boolean flag = true;
		try{
			driver.findElementById(id);
		}catch(NoSuchElementException e){
//			e.printStackTrace();
			System.out.println("没找到"+id);
			flag=false;
		}
		return flag;
	}
	public boolean isExistByName(String name){
		Boolean flag = true;
		try{
			driver.findElementByName(name);
		}catch(NoSuchElementException e){
//			e.printStackTrace();
			System.out.println("没找到"+name);
			flag=false;
		}
		return flag;
	}
	public boolean isExistByClassName(String className){
		Boolean flag = true;
		try{
			driver.findElement(By.className(className));
		}catch(NoSuchElementException e){
//			e.printStackTrace();
			System.out.println("没找到"+className);
			flag=false;
		}
		return flag;
	}
	/**
	 * pc端的键盘按键使用方法
	 * @param e1
	 * @param e2
	 */
//	public static void pressCtrlV(String string) throws AWTException, InterruptedException{
//		StringSelection stringSelection=new StringSelection(string);
//		Toolkit.getDefaultToolkit().
//			getSystemClipboard().
//				setContents(stringSelection, null);
//		Robot robot = new Robot();
//		robot.keyPress(KeyEvent.VK_CONTROL);
//		robot.keyPress(KeyEvent.VK_V);
//		robot.keyRelease(KeyEvent.VK_V);
//		robot.keyRelease(KeyEvent.VK_CONTROL);
//		Thread.sleep(1000);
//	}
//	public static void pressTab() throws AWTException, InterruptedException{
//		Robot robot = new Robot();
//		robot.keyPress(KeyEvent.VK_TAB);
//		robot.keyRelease(KeyEvent.VK_TAB);
//		Thread.sleep(1000);
//	}

	public void dragAndDrop(WebElement e1,WebElement e2){
		TouchAction ta = new TouchAction(driver);
		ta.press(e1).moveTo(e2).release().perform();
		
	}
	
	/**
	 * 按实体键的返回
	 * android专用
	 */
	public  void pressEntityBack(){
		driver.pressKeyCode(4);
	}
	/**
	 * 获取屏幕大小
	 * @return
	 */
	public int getWidth(){
		Dimension dimension;
		dimension = driver.manage().window().getSize();
		int SCREEN_WIDTH = dimension.width;
		return SCREEN_WIDTH;
	}
	public int getHeight(){
		Dimension dimension;
		dimension = driver.manage().window().getSize();
		int SCREEN_HEIGHT = dimension.height;
		return SCREEN_HEIGHT;
	}
	/**
	 * 滑动页面
	 * @param during
	 * @param num
	 */
	public   void swipeToDown(int during, int num)  {  
		int width=getWidth();
    	int height= getHeight();
	    for (int i = 0; i < num; i++) {  
	        driver.swipe( width/ 2, height* 1 / 10, width/2, height *9/ 10,during); 
	    }  
	}
	public  void swipeToUp(int during, int num) {  
		int width= getWidth();
    	int height= getHeight();
	    for (int i = 0; i < num; i++) {  
	        driver.swipe(width / 2, height * 9/ 10, width / 2, height *1/ 10,during); 
	    }  
	}
	public  void swipeToRight(int during, int num){  
		int width=getWidth();
    	int height=getHeight();
	    for (int i = 0; i < num; i++) { 
	    	driver.swipe(width*1/ 10, height /2, width * 9/ 10,
	    			height /2, during);
	    }  
	}
	public  void swipeToLeft(int during, int num){  
		int width=getWidth();
    	int height=getHeight();
	    for (int i = 0; i < num; i++) {  
	    	driver.swipe(width * 9/ 10, height /2,width*1/ 10,
	    			height /2, during);
	    }  
	}
	/**
	 * 点击we按钮的中心点
	 * @param we
	 */
	public void clickCentre(WebElement we ){
		int x = we.getLocation().x+we.getSize().width/2;
		int y = we.getLocation().y+we.getSize().height/2;
		driver.tap(1,x, y,  200);
	}
	/**
	 * 查看当前webview
	 */

	/**
	 * 切换webview
	 */
	public boolean switchToNative(){
		boolean flag =false;
		Set <String>context=driver.getContextHandles();
		if( context.contains("NATIVE_APP")){
			String currentContext=driver.getContext();
			if(!currentContext.equals("NATIVE_APP")){
				driver.context("NATIVE_APP");
				flag=true;
			}
		}
		return flag;
	}
	public boolean switchToWebView(){
		boolean flag = false;
		String currentContext=driver.getContext();
		if(currentContext.contains("NATIVE_APP")){
			((Set<String>)driver.getContextHandles()).forEach((handle)->{
				if( handle.contains("WEBVIEW")){
					System.out.println(handle);
					driver.context(handle);
				}
			});
				flag=true;
		}
		return flag;
	}
	/**
	 * 显示等待元素
	 */
	public WebElement wait4Element(int second,String xpathString){
		WebDriverWait wait=new WebDriverWait(driver, second);
		 WebElement e= wait.until(
				new  ExpectedCondition<WebElement>() {
	            @Override
	            public WebElement apply(WebDriver d) {
				return d.findElement(By.xpath(xpathString));
			}
		});
		return e;
	}
	//findElement(this,locString)
	//switchToPage
	//switchToHomepage
}
