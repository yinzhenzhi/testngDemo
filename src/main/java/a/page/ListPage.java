package a.page;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

//import com.pabys.util.Operate;

import a.model.PhoneDriver;

public class ListPage extends BasePage{
//	private PhoneDriver driver;
	public ListPage(PhoneDriver driver) {
		super(driver);
		PageFactory.initElements(driver, this);
	}
	HomePage home = new HomePage(driver);
	/**
	  * 查找serviceName,并点击
	  * 默认值false，如果找到并且点击了，就true
	  */
	 public boolean clickTarget(String serviceName){
		boolean flag = false;
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		if(isExistByaId(serviceName)){
			 WebElement we=driver.findElementByAccessibilityId(serviceName) ;
			 moveToShow(we);
			 we.click();
			 flag=true;
		}
		return flag;
	 }
	 public void moveToShow(WebElement we){
			if(we!=null){
				int top=we.getLocation().y;
				int h=getHeight();
				int w=getWidth();
				while(top>=h){
					if(top>=h){
						driver.swipe(w/2, h*9/10,w/2, h/10, 3000);
						driver.getPageSource();
						top=we.getLocation().y;
					}
				}
			}
		}
	 public boolean isThisPage(){
		 return isExistByaId("全部服务");
	 }
	 public boolean backToThisPage(){
		 int i =0;
		 boolean flag=false;
		 while (!isThisPage()){
			 if(isExistByaId("关闭")){
				 driver.findElementByAccessibilityId("关闭").click();
			 }else if(isExistByaId("用户中心")){
				 driver.findElementByAccessibilityId("全部").click();
				 flag=true;
			 }else{
				 pressEntityBack();
				 i++;
			 }
			 if(i>3){
				 break;
			 }
			 driver.getPageSource();
		 }
		 return flag;
	 }
	 public void jumpToHome(){
		 pressEntityBack();
	 }
	public boolean isLogin() {
		boolean flag;
		jumpToHome();
		flag= home.isLogin();
		home.jumpToList();
		return flag;
	}
	public void logout() {
		jumpToHome();
		home.logout();
		home.jumpToList();
	}
	public boolean login(String username,String password){
		boolean flag;
		jumpToHome();
		flag = home.login(username, password);
		home.jumpToList();
		return flag;
	}
}
