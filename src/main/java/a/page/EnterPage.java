package a.page;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import com.pabys.info.EnvInfo;
//import com.pabys.util.Operate;
//import com.thoughtworks.selenium.webdriven.commands.DragAndDrop;

import a.model.PhoneDriver;
import a.page.BasePage;
public class EnterPage extends BasePage{
	private static final String env=EnvInfo.env;
	private static final String entraceName = EnvInfo.entraceName;
	private static final String Login=EnvInfo.loginUrl;
	private static final String URL = EnvInfo.URL;
	
//	private PhoneDriver driver;
//	public EnterPage(PhoneDriver driver){
//		super(driver);
//		if(driver!=null){
//			PageFactory.initElements(driver, this);
//			System.out.println("？？？--");
//		}else{
//			System.out.println("driver is null");
//		}
//	}
	/**
	 * 第一步，启动后选择环境，点击loading
	 * 第二部，点击宿主登出，
	 * 第三部，等待5s，右滑下方查看页面登录入口展示，点击进入用户中心
	 * @param driver
	 * @throws InterruptedException
	 */
	 @FindBy(xpath="//android.widget.Spinner")///android.widget.TextView[@text='测试环境']/parent::/android.widget.Spinner
	 private WebElement  chooseEnv;
	 
	 @FindBy(name=env)
	 private WebElement env_1st;
	 
//	 @FindBy(id="android:id/button2")
//	 private WebElement refuse;
	 
	 @FindBy(id = "com.paic.example.simpleapp:id/loading")
	 private WebElement loading;
	 public EnterPage(PhoneDriver driver) {
		// TODO Auto-generated constructor stub
		 super(driver);
	}

	/**
	  * 第一步，点击loading
	 * @throws InterruptedException 
	  */
	 private void _1stPage() throws InterruptedException{
		Thread.sleep(5000);
		/*String  pagesource=driver.getPageSource();
		String []psStrings=pagesource.split("resource-id");
		for(String ps:psStrings){
			System.out.println(ps);
		}   */
//		WebElement webElement = wait4Element(30, "//android.widget.Button[contains(@text,'loading')]");
//		System.out.println(webElement.getText());
		loading.click();
//		if(driver==null){
//			System.out.println("driver is null");
//		}else{
//			if(driver.findElementById("com.paic.example.simpleapp:id/loading").isDisplayed()){
//				driver.findElementById("com.paic.example.simpleapp:id/loading").click();
//			}else{
//				System.out.println("haven't find the element");
//			}
//		}
	 }
	 
	 /**
	  * 第2步,点击宿主登出
	  * @param driver
	  * @throws InterruptedException
	  */
//	 @FindBy(name ="宿主登出")
	 @FindBy(xpath="//android.widget.Button[@text='宿主登出']")
	 private WebElement logOutButton;
	 //1.7.1desktop不识别name
	 //findElementByAndroidUIAutomator("new UiSelector().text(\"+<your_text>+\")");
	 private void _2ndPage(){
		 try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 logOutButton.click();
	 }
		
//	@AndroidFindBy(uiAutomator = "new UiSelector().description(\"登录\")")
//	public MobileElement loginButton;
	@FindBy(xpath="//android.view.View[@content-desc='登录']")
	private WebElement loginButton;

//	public MobileElement me;
//	me=driver.findElementByUI
	 /**
		 * 第3步，左滑页面，点击用户中心
		 * @param driver
		 * @throws InterruptedException
		 */
	private void _3rdPage() throws InterruptedException{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Thread.sleep(10000);
		driver.swipe(100, getHeight() - 10, getWidth() - 10,getHeight() - 10, 200);
		while (loginButton.getLocation().x<0) {
			System.out.println(loginButton.getLocation().x);
			driver.getPageSource();
			Thread.sleep(3000);
		}
		driver.tap(1, 1, 200,2000);
		loginButton.click();
	 }
	 /**
	  * 选取环境页面
	  * @param driver
	  * @throws InterruptedException
	  */
	 @FindBy(name = URL)
	 private WebElement linkURL;
	 
	 /**
	  * 第4步，如果有stg环境选择页面，选择对应环境
	  */
	 private void _4thPage(){
		 driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		 
		 try{
			 linkURL.click();
			 }catch(NoSuchElementException e){
				 System.out.println("没有选择环境入口，直接进入用户中心");
			 }
	 }

	public void enterPage() throws InterruptedException{
		_1stPage();
		_2ndPage();
		_3rdPage();
		_4thPage();
	}
	public void logout(){
		while (!isExistByaId("宿主登出")){
			if(isExistByaId("loading")){
				loading.click();
			}else{
				pressEntityBack();
			}
		}
		logOutButton.click();
	}
	public void jumpToUserCenter(){
		try {
			_3rdPage();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_4thPage();
	}
}
//chooseEnv.click();
//env_1st.click();
/*		 driver.getContext()//NATIVE_APP
driver.currentActivity()//main.setting.SettingActivity
driver.getCurrentUrl()//网页专用
driver.getCapabilities()//capabilities的所有设置
driver.getContextHandles()//NATIVE_APP
driver.startActivity(??????,?????)
driver.context(????)*/

//TODO 系统提示窗的点击
//if(isExistByaId("拒绝")){
//	 driver.findElementByAccessibilityId("拒绝").click();
//}