package a.model;
//import org.apache.log4j.Logger;
public class PhoneDriverFac {
	private static PhoneDriver driver=null;
//	private static Logger logger=Logger.getLogger(PhoneDriverFac.class);
	
	//单例模式
	public static PhoneDriver getDriver() {
		if(driver==null){
//				logger.info("driver==null,创建PaDriver");
				driver=new PhoneDriver();
		}
		return driver;
	}
	private PhoneDriverFac() {
	}
	
}
