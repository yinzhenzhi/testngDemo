package a.model;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import com.pabys.info.EnvInfo;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
public class PhoneDriver extends AndroidDriver{
	private static String LINKURL="http://127.0.0.1:4723/wd/hub";
	private static URL initUrl;
	private static String uuid=EnvInfo.getUdid();
	private static String osVersion=EnvInfo.getOsVersion();
	private static URL getInitUrl(){
		try {
			initUrl = new URL(LINKURL);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		logger.info("link:"+initUrl);
		return initUrl;
	}
	
	public PhoneDriver() {
		super(getInitUrl(),info());
	}
	//TODO during时间没有
/*	@Override
	public void swipe(int startx,int starty,int endx,int endy,int duration){
		super.swipe(startx, starty, endx, endy, duration);;
//		TouchAction tAction = new TouchAction(this);
//		int valueX=endx-startx;
//		int valueY=endy-starty;
//		tAction.press(startx,starty).moveTo(valueX,valueY).release().perform();
	}
	//TODO finger 和during没有
	@Override
	public void tap(int fingers,int x,int y ,int duration){
		super.tap(fingers, x, y, duration);
//		new TouchAction(this).press(x,y).perform();
	}*/
	
	
	
	private static DesiredCapabilities info(/*File app,MobileInfo minfo*/) {
		File classpathRoot = new File(System.getProperty("user.dir"));
		File appDir = new File(classpathRoot, "app");
		File app = new File(appDir,EnvInfo.APPNAME);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("appPackage", "com.paic.example.simpleapp");//参数没问题
		//微信专用
//		DesiredCapabilities option = new DesiredCapabilities();
//		option.setCapability("androidProcess", "com.xxxxx");
//		capabilities.setCapability(ChromeOptions.CAPABILITY, option.ToDictionary());
		//为了切换webview
		capabilities.setCapability("recreateChromeDriverSessions", true);
//		capabilities.setCapability("appPackage", "com.paic.example.simpleapp");
		
		
		capabilities.setCapability(CapabilityType.PLATFORM,
				"Android");
		//修改plat_form
		capabilities.setCapability(MobileCapabilityType.UDID, uuid);
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "device"); 
		capabilities
				.setCapability(MobileCapabilityType.PLATFORM_VERSION, osVersion); 
		capabilities.setCapability(MobileCapabilityType.APP,
				app.getAbsolutePath()); //这一条 指定了app的安装路径
		capabilities.setCapability("noReset", true);
//		logger.info("赋值完毕");
		return capabilities;
	}
}
