package b.testng.reportRelated;

import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;
public class ListenerTest extends TestListenerAdapter{
	private Logger logger = Logger.getLogger(ListenerTest.class);
	private int m_count = 0;
	 	
	@Override
	public void onTestStart(ITestResult result) {
		// TODO Auto-generated method stub
		super.onTestStart(result);
		logger.info("["+result.getName()+"start]");
	}
	
	  @Override
	  public void onTestFailure(ITestResult tr) {
		  super.onTestFailure(tr);
		  logger.info("["+tr.getName()+" Failure ]");
	    log("F");
	  }
	 
	  @Override
	  public void onTestSkipped(ITestResult tr) {
		  super.onTestSkipped(tr);
		  logger.info("["+tr.getName()+" skipped ]");
	    log("S");
	  }
	 
	  @Override
	  public void onTestSuccess(ITestResult tr) {
		  super.onTestSuccess(tr);
		  logger.info("["+tr.getName()+" success]");
	    log(".");
	  }
	 
	  @Override
	  public void onFinish(ITestContext testContext){
		  super.onFinish(testContext);
	  }
	  private void log(String string) {
	    System.out.print(string);
	    if (++m_count % 40 == 0) {
	      System.out.println("");
	    }
	  }

}
