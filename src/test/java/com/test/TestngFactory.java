package com.test;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;

public class TestngFactory {  
    @Factory (dataProvider="account_Name") 
    //这个方法是用例的创建实例的方法
    public Object[] createInstances(int a,int b) {
    	System.out.println("a:"+a+",b:"+b);
    	int sub=Math.abs(a-b)+1;
    	Object[]result = new Object[sub];
    	
        for(int i = 0; i <sub; i++) {
        	String iString =String.valueOf(a+i);
        	result[i] = new  FactoryTest("test"+iString);  
        }
        return result;  
    }
    @DataProvider(name = "account_Name")
    public Object[][] accountName(){
    	return new Object[][]{{new Integer(1994),new Integer(1998)},{new Integer(5),new Integer(10)}};
    }
}
