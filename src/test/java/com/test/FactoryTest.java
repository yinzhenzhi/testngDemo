package com.test;

import org.testng.annotations.Test;

public class FactoryTest {
	 private String accountName;  
	    public  FactoryTest(String accountName) {  
	        this.accountName = accountName;  
	    }  
	    @Test  
	    public void testServer() {  
	        System.out.println("accountName ："  
	                + accountName);  
	    }  
}
