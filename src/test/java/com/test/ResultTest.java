package com.test;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

public class ResultTest {
	@Test
	public void verifyTrue1(){
		assert "hello".equals("hello");
	}
	@Test
	public void verifyFalse1(){
		assert "hello".equals("hello1");
	}
	@Test
	public void verifyTrue2(){
		assertEquals("real", "real");
	}
	@Test
	public void verifyFalse2(){
		assertEquals("actual", "expect");
	}
}

