package com.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.Parser;
import org.testng.xml.XmlSuite;

public class RuntestngTestsuite {
	public static void main(String[] args) {
		
		TestNG tng = new TestNG();
		RetryTestListener rtl = new RetryTestListener();
		XmlSuite xs = new XmlSuite();
		Parser parser = new Parser("./testxml/temp.xml");
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		try {
		suites = parser.parseToList();
		} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		};
		tng.setXmlSuites(suites);
		tng.addListener(rtl);
		tng.run();
	}
}
