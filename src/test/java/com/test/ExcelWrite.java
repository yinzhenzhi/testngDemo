package com.test;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.HorizontalAlignment;  
public class ExcelWrite {
	    public static void main(String[] args) {  
	    	ExcelWrite obj = new ExcelWrite();  
	        obj.writeExcel();  
	    }  
	    private void writeExcel() {  
	    	HSSFWorkbook wb = new HSSFWorkbook();  
	    	HSSFSheet sheet = wb.createSheet("sheet_page1");  
	    	HSSFRow row = sheet.createRow(0);  
	    	//第四步，创建单元格，并设置值表头 设置表头居中
	    	HSSFCellStyle style = wb.createCellStyle();  
	        style.setAlignment(HorizontalAlignment.CENTER); // 创建一个居中格式  
	        // 设置表头行各个列的名字，  
	        setSheetHeader(row, style);  
	        
	        List<Person>list = getStudentDatas();
	        insertDataToSheet(sheet,list);
	        
	        writeExcelToDisk("C:/Users/user/Desktop/export.xlsx", wb);
	}
	    private void writeExcelToDisk(String filePath, HSSFWorkbook wb) {
			// TODO Auto-generated method stub
	    	try {  
	            FileOutputStream fout = new FileOutputStream(filePath);  
	            wb.write(fout);  
	            fout.close();  
	            System.out.println("excel已经导出到:" + filePath);  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
		}
		private void insertDataToSheet(HSSFSheet sheet, List<Person> list) {
	    	 HSSFCell cell = null;  
	    	    HSSFRow row = null;  
	    	    for (int i = 0; i < list.size(); i++) {
	    	    	//创建列
	    	        row = sheet.createRow(i + 1);  
	    	        Person stu = list.get(i);  
	    	        // 创建单元格，并设置各个列中实际数据的值  ，参数是column
	    	        cell = row.createCell((short) 0);  
//	    	        cell.setEncoding(HSSFCell.ENCODING_UTF_16);  
	    	        cell.setCellValue(stu.getId());  
	    	  
	    	        cell = row.createCell((short) 1);  
//	    	        cell.setEncoding(HSSFCell.ENCODING_UTF_16);  
	    	        cell.setCellValue(stu.getName());  
	    	  
	    	        cell = row.createCell((short) 2);  
//	    	        cell.setEncoding(HSSFCell.ENCODING_UTF_16);  
	    	        cell.setCellValue(stu.getAge());  
	    	    }  
		}
		private List<Person> getStudentDatas() {
			// TODO Auto-generated method stub
			List<Person> list =new ArrayList();
			Person p1 = new Person(001, "张三", 12);
			Person p2 = new Person(002, "张四", 13);
			list.add(p1);
			list.add(p2);
			return list;
		}
		/**
	     * 设置表头行各个列的名字
	     * @param row
	     * @param style
	     */
		private void setSheetHeader(HSSFRow row, HSSFCellStyle style) {
			 HSSFCell cell = row.createCell((short) 0);  
			    cell.setCellStyle(style);  
			    cell.setCellValue("学号");  
			  
			    cell = row.createCell((short) 1);  
			    cell.setCellValue("姓名");  
			    cell.setCellStyle(style);  
			  
			    cell = row.createCell((short) 2);  
			    cell.setCellValue("年龄");  
			    cell.setCellStyle(style);  
		}  
	    
}
class Person {  
    private int id;  
    private String name;  
    private int age;  
  
    public Person(int id, String name, int age) {  
        super();  
        this.id = id;  
        this.name = name;  
        this.age = age;  
    }  
  
    public int getId() {  
        return id;  
    }  
  
    public void setId(int id) {  
        this.id = id;  
    }  
  
    public String getName() {  
        return name;  
    }  
  
    public void setName(String name) {  
        this.name = name;  
    }  
  
    public int getAge() {  
        return age;  
    }  
  
    public void setAge(int age) {  
        this.age = age;  
    }  
  
}  
