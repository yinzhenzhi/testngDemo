package com.test;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

public class RunTestng {
	public static void main(String[]args){
	TestListenerAdapter tla =new TestListenerAdapter();
	TestNG testng= new TestNG();
	//普通测试类
	testng.setTestClasses(new Class[]{AppTest.class});
	testng.addListener(tla);
	testng.run();
	}
}