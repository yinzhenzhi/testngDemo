package com.test;

import java.io.IOException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailTest {
	private static String fromEmail="yindongzi@163.com";
	private static String fromId="dongzi";
	private static String fromPassword="yinzhenzhi";
	private static String emailSMTPHost="smtp.163.com";
	private static String toEmail = "yinzhenzhi246@pingan.com.cn";
	private static String toID="接收人";
	private static String toTitle="this is title";
	private static String toContent="this is content";
	private static String WORD_CODE="utf-8";
	
	public static void main(String[] args) throws MessagingException, IOException {
		//set up configuration 
		Properties props=new Properties();
		props.setProperty("mail.transport.protocol", "smtp");
		props.setProperty("mail.smtp.host", emailSMTPHost);
		props.setProperty("mail.smtp.auth", "true");
       
		/* SSL安全连接
        * final String smtpPort = "465";
        props.setProperty("mail.smtp.port", smtpPort);
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.socketFactory.port", smtpPort);*/
		
		//set up session,contact with other
		Session session=Session.getInstance(props);
		session.setDebug(true);
		
		//create a email
		MimeMessage message=createMimeMessage(session,fromEmail,toEmail);
		
		//get the email transfer instance according to the session
		Transport transport =session.getTransport();
		transport.connect(fromEmail,fromPassword);
		
		transport.sendMessage(message, message.getAllRecipients());
		
		transport.close();
		
	}

	private static MimeMessage createMimeMessage(Session session, String fromEmail2, String toAccount2) throws MessagingException, IOException {
		MimeMessage message = new MimeMessage(session);
		//from 发件人
		message.setFrom(new InternetAddress(fromEmail,fromId,WORD_CODE));
		//TO 收件人
		message.setRecipient(RecipientType.TO,
		new InternetAddress(toEmail,toID,WORD_CODE));
		//subject 邮件主题
		message.setSubject(toTitle,WORD_CODE);
		//content 邮件正文
		message.setContent(toContent,WORD_CODE);
		//date 发件时间
		message.setSentDate(new Date());
		//save 保存前面的设置
		message.saveChanges();
		return message;
	/*	//save 保存邮件到本地
		OutputStream outputStream = new FileOutputStream("myEmail.eml");
		message.writeTo(outputStream);
		outputStream.flush();
		outputStream.close();*/
	}
}
