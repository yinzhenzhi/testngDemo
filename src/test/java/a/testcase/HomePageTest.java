package a.testcase;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import a.model.PhoneDriver;
import a.page.EnterPage;
import a.page.HomePage;
import a.page.RegisterPage;
public class HomePageTest  extends BaseTest{
	
	
	@BeforeMethod
	public void checkPage() throws InterruptedException{
//			Assert.assertTrue(home.backtoThisPage());
		home.backtoThisPage();
//		System.out.println("title is :"+driver.getTitle());//这个方法会报错
		
/*		while(!home.isExistByaId("用户中心")){
			home.pageSource();
			if(home.isExistByaId("关闭")){
				driver.findElementByAccessibilityId("关闭").click();
			}else{
//				System.out.println("???");
				home.pressEntityBack();
			}
		}
		Thread.sleep(3000);*/
	}

	@DataProvider(name="serviceUnlogin")
	public Object[][] service1Detail(){
	    return new Object[][]{
	    	{"我的游戏","更多游戏","登录"},
	    	{"登录轨迹","查看更多","登录"},
	    	{"我的彩票","","登录"},
	    	{"万里通积分","","登录"},
	    	{"预约挂号","登录预约","登录"},
	    	{"生活缴费","","登录"}, 
	    	{"我的资产","立即查看","登录"},
	    	{"问律师","立即咨询","登录"},
	    	{"手机维修","","登录"},
	    	{"家电服务","家电清洗","登录"},
	    	{"家电服务","家电维修","登录"},
	    	{"家电服务","家电安装","登录"},
	    	{"房产大咖","好房新闻","更多大咖说房"},
	    	{"精选房专题","新房优选【平安好房】","进入专题"},
	    	{"热门资讯","更多","热点"},
	    	{"轻松一刻","更多笑话","轻松一刻"},
	    	{"股市行情","更多行情","股票行情"},
	    	{"附近","探索您身边的平安服务", "附近的平安服务"},
	    	};
	    }
	@DataProvider(name="serviceLogin")
	public Object[][] service2Detail(){
	    return new Object[][]{
//	    	{"我的游戏","更多游戏","积分游戏"},
	    	{"登录轨迹","查看更多","用户登录轨迹"},
	    	{"我的彩票","","玩游戏赢好礼"},
	    	{"万里通积分","","积分团购"},
	    	{"预约挂号","登录预约", "平安好医生"},
	    	{"生活缴费","","生活缴费"}, 
	    	{"我的资产","立即查看","一账通账户资产查询"},
	    	{"问律师","婚姻家庭","闪电律师"},
	    	{"问律师","房产物业","闪电律师"},
	    	{"问律师","民间借贷","闪电律师"},
	    	{"问律师","交通意外","闪电律师"},
	    	{"手机维修","","极客修"},
	    	{"家电服务","家电清洗","返回"},
	    	{"家电服务","家电维修","返回"},
	    	{"家电服务","家电安装","返回"},
	    	};
	    }
	private void test(boolean flag){
		Assert.assertTrue(flag);
	}
	private void check(String serviceName, String clickName, String resultTitle) {
		test(home.clickTarget(serviceName, clickName));
		test(home.isExistByName(resultTitle));
	}
//	@Test(dataProvider="serviceUnlogin",alwaysRun=false)
//	public void testService(String serviceName,String clickName,String resultTitle){
//		if(home.isLogin()){
//			test(home.logout());
//			}
//		check(serviceName, clickName, resultTitle);
//	}
	
	@Test(dataProvider="serviceLogin")
	public void testService2(String serviceName,String clickName,String resultTitle){
		if(!home.isLogin()){
			test(home.login("test1998","qwer1234"));
			}
		check(serviceName, clickName, resultTitle);
	}
}
