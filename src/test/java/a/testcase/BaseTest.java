package a.testcase;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import a.model.PhoneDriver;
import a.page.EnterPage;
import a.page.HomePage;
import a.page.ListPage;
import a.page.RegisterPage;

public class BaseTest {
	PhoneDriver driver = new PhoneDriver();
	EnterPage enter = new EnterPage(driver);
	HomePage home = new HomePage(driver);
	RegisterPage register= new RegisterPage(driver);
	ListPage list = new ListPage(driver);
	@BeforeSuite
	public void setUp() throws InterruptedException{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		enter.enterPage();

	}
	@AfterSuite
	public void tearDown(){
		enter=null;
		home=null;
		register=null;
		list=null;
		if(driver!=null){
			driver.quit();
		}
		driver=null;
	}
}
