package a.testcase;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import a.model.PhoneDriver;
import a.page.EnterPage;
import a.page.HomePage;
import a.page.ListPage;
import a.page.RegisterPage;

public class ListPageTest extends BaseTest{
	//TODO 测试一下把所有的page单例化，将构造函数private，将getXXX函数public，所有涉及页面的new，都用getXXX代替
	boolean isLogin=list.isLogin();
	@BeforeClass
	public void setUp() throws InterruptedException{
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(home.isThisPage()&&(!home.isExistByaId("请登录"))){
				home.logout();
		}else{
			home.jumpToList();
		}
	}
	@AfterClass
	public void quit(){
		driver.quit();
	}
	@BeforeMethod
	public void checkPage(){
		Assert.assertTrue(list.backToThisPage(),"fail to back to the page");
		/*while(!home.isExistByaId("用户中心")){
			home.pageSource();
			if(home.isExistByaId("关闭")){
				driver.findElementByAccessibilityId("关闭").click();
			}else{
				home.pressEntityBack();
			}
		}*/
	}

	@DataProvider(name="serviceUnlogin")
	public Object[][] service1Detail(){
	    return new Object[][]{
	    	{"我的游戏","登录"},
	    	{"登录轨迹","登录"},
	    	{"我的彩票","登录"},
	    	{"万里通积分","登录"},
	    	{"预约挂号","登录"},
	    	{"生活缴费","登录"}, 
	    	{"我的资产","登录"},
	    	{"问律师","登录"},
	    	{"手机维修","登录"},
	    	{"家电服务","登录"},
	    	{"家电服务","登录"},
	    	{"家电服务","登录"},
	    	{"房产大咖","好房新闻"},
	    	{"精选房专题","新房优选【平安好房】"},
	    	{"热门资讯","热点"},
	    	{"轻松一刻","轻松一刻"},
	    	{"股市行情","股票行情"},
	    	{"附近","附近的平安服务"},
	    	};
	    }
	@DataProvider(name="serviceLogin")
	public Object[][] service2Detail(){
	    return new Object[][]{
	    	{"我的游戏","积分游戏"},
	    	{"登录轨迹","用户登录轨迹"},
	    	{"我的彩票","玩游戏赢好礼"},
	    	{"万里通积分","积分团购"},
	    	{"预约挂号", "平安好医生"},
	    	{"生活缴费","生活缴费"}, 
	    	{"我的资产","一账通账户资产查询"},
	    	{"问律师","闪电律师"},
	    	{"手机维修","极客修"},
	    	{"家电服务","十分到家"},
	    	};
	    }
	private void check(String serviceName, String resultTitle) {
			Assert.assertTrue(list.clickTarget(serviceName));
			Assert.assertTrue(home.isExistByName(resultTitle));
	}
	@Test(dataProvider="serviceUnlogin")
	public void testService(String serviceName,String resultTitle){
		if(isLogin){
			list.logout();
			isLogin=list.isLogin();
		}
		check(serviceName, resultTitle);
	}
	
	
	@Test(dataProvider="serviceLogin")
	public void testService2(String serviceName,String clickName,String resultTitle){
		if(!isLogin){
			list.login("test1998","qwr1234");
			isLogin=list.isLogin();
		}
		check(serviceName,resultTitle);
	}
}
